def solve_exercise_1(emails_list):
    print(len(emails_list))
    print("First element: ", emails_list[0])
    print("Last element", emails_list[-1])
    emails_list.append("cde@example.com")
    print(emails_list)


def generate_email(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"


def make_list_of_tripled_numbers(number1, number2, number3):
    return [number1 * 3, number2 * 3, number3 * 3]


def average_students_marks(marks_list):
    not_rounded_average = sum(marks_list) / len(marks_list)
    rounded_average = round(not_rounded_average, 1)
    return rounded_average


if __name__ == '__main__':
    # Exercise 1
    emails = ["a@example.com", "b@example.com"]
    solve_exercise_1(emails)

    # Exercise 2
    janusz_email = generate_email("Janusz", "Nowak")
    barbara_email = generate_email("Barbara", "Kowalska")
    print(janusz_email)
    print(barbara_email)

    # Exercise 3
    numbers_tripled_list = make_list_of_tripled_numbers(4, 5, 8)
    print(numbers_tripled_list)

    # Exercise 4
    student_marks_a = [5, 3, 2, 4, 1, 1, 3, 5, 4]
    print(f"Student A grade point average is:", average_students_marks(student_marks_a))
