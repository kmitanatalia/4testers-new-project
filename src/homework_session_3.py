def gamer_info_printer(player_data):
    player_description = f"The player {player_data['nick']} is of type {player_data['type']} and has {player_data['exp_points']} EXP"
    print(player_description)


def number_divisible_of_another_number_finder(number_from, number_to, divisible_by):
    list_of_num = []
    for n in range(number_from, number_to + 1):
        if n % divisible_by == 0:
            list_of_num.append(n)
    return list_of_num


def convert_temperatures_in_celsius_to_fahrenheit(list_of_temps_in_celsius):
    fahrenheit_temperatures = []
    for t_celsius in list_of_temps_in_celsius:
        t_fahrenheit = round(t_celsius * 1.8 + 32, 2)
        fahrenheit_temperatures.append(t_fahrenheit)
    return fahrenheit_temperatures


if __name__ == '__main__':
    # Exercise 1
    player_details = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": "3000"
    }

    player_2_details = {
        "nick": "mr_boogie_123",
        "type": "mage",
        "exp_points": "67890"
    }

    gamer_info_printer(player_details)
    gamer_info_printer(player_2_details)

    # Exercise 2
    print(number_divisible_of_another_number_finder(0, 27, 9))

    # Exercise 3
    print(convert_temperatures_in_celsius_to_fahrenheit([10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]))
