from car import Car

def test_drive_increases_car_mileage():
    test_car = Car ("Mazda", "black", "2005")
    test_car.drive(100)
    test_car.drive(100)
    assert test_car.mileage == 200

 def test_get_age_of_a_car():
     test_car = Car ("Mazda", "black", "2005")
     assert test_car.get_age() == 29

 def test_repaint_the_car():
     test_car = Car ("Mazda", "black", "2005")
     test_car.repaint("Yellow")
     assert test_car.color =="Yellow"
