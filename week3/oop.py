class Dog:

    def __init__(self, name):
        self.name = name


if __name__ == '__main__':
    dog1 = Dog("Azor")
    dog2 = Dog("Burek")
    print(dog1.name)
    print(dog2.name)