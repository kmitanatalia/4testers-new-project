import datetime


class Car:

    def __init__(self, brand, color, production_year):
        self.brand = brand
        self.color = color
        self.production_year = production_year
        self.mileage = 0

    def drive(self,distance):
        self.mileage += distance

    def get_age(self):
        return datetime.datetime.now().year - self.production_year

    def repaint(self,  color):
        self.color = color


if __name__ == '__main__':
    car1 = Car("Mazda", "black", "2005")
    car2 = Car("BMW", "red", "2023")
    car3 = Car("Mercedes", "white", "2017")
